<?php

use yii\db\Migration;

/**
 * Class m180424_024327_init
 */
class m180424_024327_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(10)
                ->comment('Идентификатор пользователя'),
            'name' => $this->string(255)
                ->notNull()
                ->comment('Имя пользователя'),
            'balance' => $this->decimal(10, 2)
                ->unsigned()
                ->defaultValue(0)
                ->notNull()
                ->comment('Баланс пользователя')
        ]);

        $this->createTable('transaction', [
            'id' => $this->primaryKey()
                ->comment('Идентификатор транзакции'),
            'created_at' => $this->timestamp()
                ->defaultExpression('NOW()'),
            'sender_id' => $this->integer(10)
                ->notNull()
                ->comment('Идентификатор отправителя'),
            'recipient_id' => $this->integer(10)
                ->notNull()
                ->comment('Идентификатор получателя'),
            'amount' => $this->decimal(10, 2)
                ->unsigned()
                ->notNull()
                ->comment('Сумма перевода')
        ]);

        $this->addForeignKey(
            'fk-transaction-sender',
            'transaction',
            'sender_id',
            'user',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-transaction-recipient',
            'transaction',
            'recipient_id',
            'user',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        $this->dropForeignKey('fk-transaction-recipient', 'transaction');
        $this->dropForeignKey('fk-transaction-sender', 'transaction');
        $this->dropTable('transaction');
        $this->dropTable('user');

        return true;
    }
}
