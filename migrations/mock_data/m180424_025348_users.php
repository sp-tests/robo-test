<?php

use yii\db\Migration;

/**
 * Class m180424_025348_mock_data
 */
class m180424_025348_users extends Migration
{
    private static $users = [
        [
            'name' => 'Вася',
            'balance' => 1000.50
        ],
        [
            'name' => 'Петя',
            'balance' => 500.1
        ],
        [
            'name' => 'Ваня',
            'balance' => 0
        ],
        [
            'name' => 'Илья',
            'balance' => 300.40
        ],
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        foreach (self::$users as $user) {
            $this->insert('user', $user);
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        echo "m180424_025348_users cannot be reverted.\n";

        return false;
    }
}
