<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id Идентификатор транзакции
 * @property string $created_at
 * @property int $sender_id Идентификатор отправителя
 * @property int $recipient_id Идентификатор получателя
 * @property string $amount Сумма перевода
 *
 * @property User $recipient
 * @property User $sender
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['sender_id', 'recipient_id', 'amount'], 'required'],
            [['sender_id', 'recipient_id'], 'integer'],
            [['amount'], 'number', 'min' => 0.01],
            [['recipient_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['recipient_id' => 'id'], 'message' => 'Recipient not found'],
            [['sender_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['sender_id' => 'id'], 'message' => 'Sender not found'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'sender_id' => 'Sender ID',
            'recipient_id' => 'Recipient ID',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecipient()
    {
        return $this->hasOne(User::class, ['id' => 'recipient_id'])
            ->from(['recipient' => User::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(User::class, ['id' => 'sender_id'])
            ->from(['sender' => User::tableName()]);
    }
}
